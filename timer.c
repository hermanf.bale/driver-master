#include "timer.h"

void timer(void){
    clock_t begin;
    double timeSpent;
    *p_timerFinished = 0;

    begin = clock();
    while(1){
        timeSpent = (double)(clock() - begin)/CLOCKS_PER_SEC;
        if(timeSpent >= 3.0){
            *p_timerFinished = 1;
            break;
        }
    }    
}
