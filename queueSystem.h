#pragma once

#include "elevator.h"
#include "elevio.h"

int g_queue[N_BUTTONS][N_FLOORS];

int endDest = 4;
int *p_endDest = &endDest;

void initQueue(void);

void addToQueue(int b, int f);
void removeFromQueue(int b);
void flushQueue(void);
int checkFlush(void);
void updateQueue(void);
void checkFloorUP(void);
void checkFloorDOWN(void);
void setEndDest(void);
