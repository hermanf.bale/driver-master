#include "queueSystem.h"
#include "FSM.h"


void initQueue(void){
    for(int b=0;b<N_BUTTONS;b++){
        for(int f=0;f<N_FLOORS;f++){
            *(*(g_queue+b)+f)=0;
        }
    }
}

void flushQueue(void){
    initQueue();
}

int checkFlush(void){
    for(int b=0;b<N_BUTTONS;b++){
        for(int f=0;f<N_FLOORS;f++){
            if(*(*(g_queue+b)+f) == 1){
                return 0;
            }
        }
    }
    return 1;
}

void addToQueue(int b, int f){
    int addFloor=-1;
    int *p_addFloor=&addFloor;

    *p_addFloor = elevio_callButton(f,b);
    if(addFloor!=-1){
        *(*(g_queue+b)+f)=1;
        elevio_buttonLamp(f,b,*p_addFloor);     
    }
                       
}

void removeFromQueue(int b){
    if(*p_state == OPEN){
        *(*(g_queue+b)+*p_currentFloor) = 0;
        elevio_buttonLamp(*p_currentFloor,b,0);               
    }
}

void updateQueue(void){
    for(int b=0;b<N_BUTTONS;b++){
        for(int f=0;f<N_FLOORS;f++){
            addToQueue(b,f);                    
            removeFromQueue(b);  
        }
    }                 
}



void checkFloorUP(void){          
    if(*(*(g_queue+BUTTON_HALL_UP)+*p_currentFloor)==1){
        embark();
    } 
    if(*p_endDest == *p_currentFloor){
        setEndDest();
        embark();
    }   
}

void checkFloorDOWN(void){
    if(*(*(g_queue+BUTTON_HALL_DOWN)+*p_currentFloor)==1){
        embark();
    }
    if(*p_endDest == *p_currentFloor){
        setEndDest();
        embark();
    } 
}


void setEndDest(void){
    static int prioOne = 2;
    static int prioTwo = 1;
    static int prioThree = 0;
    
    int flushed = checkFlush();
    if(flushed == 1){
        *p_endDest = 4;
    }
    else{
        for(int f=0;f<N_FLOORS;f++){
            if(*(*(g_queue+prioThree)+f)== 1){
                *p_endDest = f;
            }
            if(*(*(g_queue+prioTwo)+f)== 1){
                *p_endDest = f;
            }
            if(*(*(g_queue+prioOne)+f) == 1){
                *p_endDest = f;
                break;
            }
        }
    }
}