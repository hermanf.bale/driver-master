#pragma once
#include "elevio.h"

int g_doorOpen;
int *p_doorOpen = &g_doorOpen;

int g_currentFloor; 
int *p_currentFloor = &g_currentFloor;

int g_obstructed;
int *p_obstructed = &g_obstructed;

int g_stop;
int *p_stop = &g_stop;

void initialize(void);
void embark(void);
void closeDoor(void);
void stopper(void);

