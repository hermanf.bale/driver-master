#include "FSM.h"
#include "elevator.h"
#include "elevio.h"
#include "timer.h"
#include "queueSystem.h"

void FiniteStateMachine(void){

    setEndDest();

    while(1){
        *p_currentFloor = elevio_floorSensor();
        *p_obstructed = elevio_obstruction();
        *p_stop = elevio_stopButton();

        updateQueue();

        if(*p_currentFloor != -1){
            elevio_floorIndicator(*p_currentFloor);
        }

        switch (*p_state){
            case STAT:
                if(*p_stop == 1){
                    stopper();
                    break;
                }
                if(*p_doorOpen){
                    *p_state = OPEN;
                    break;
                }
                if(*p_endDest == 4){
                    break;
                }
                if(*p_endDest < *p_currentFloor){
                    elevio_motorDirection(DIRN_DOWN);
                    *p_state = DOWN;
                    break;
                }
                if(*p_endDest > *p_currentFloor){
                    elevio_motorDirection(DIRN_UP);
                    *p_state = UP;
                    break;
                }
                break;

            case OPEN:
                if(*p_timerFinished==1){
                    closeDoor();
                    *p_state = STAT;
                    break;
                }
                if(*p_obstructed == 1 || *p_stop == 1){
                    timer();
                    *p_state = OPEN;
                    break;
                }
                break;

            case DOWN:
                if(*p_stop == 1){
                    *p_state = STAT;
                    stopper();
                    break;
                }
                if(*p_currentFloor != -1){
                    checkFloorDOWN();
                    break;
                }
                break;

            case UP:
                if(*p_stop == 1){
                    *p_state = STAT;
                    stopper();
                    break;
                }
                if(*p_currentFloor != -1){
                    checkFloorUP();
                    break;
                }
                break;

            default:
                *p_state = STAT;
                break;
        }
    }
}