#pragma once

typedef enum{
    STAT    = 0,
    OPEN    = 1,
    DOWN    = 2,
    UP      = 3
} STATE;

STATE state = 0;
STATE *p_state = &state;

void FiniteStateMachine(void);