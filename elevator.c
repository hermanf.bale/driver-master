#include "elevator.h"
#include "elevio.h"
#include <stdlib.h>
#include "timer.h"
#include "FSM.h"

void initialize(void){
    *p_doorOpen = 0;
    elevio_motorDirection(DIRN_DOWN);
    while(*p_currentFloor == -1){
        continue;
    }
    elevio_motorDirection(DIRN_STOP);
}

void embark(void){
    elevio_motorDirection(DIRN_STOP);
    *p_state = STAT;
    elevio_doorOpenLamp(1);
    *p_doorOpen = 1;
    timer();
}

void closeDoor(void){
    elevio_doorOpenLamp(0);
    *p_doorOpen = 0;
}

void stopper(void){
    elevio_motorDirection(DIRN_STOP);
    flushQueue();
    while(*p_stop){
        elevio_stopLamp(1);
    }
    elevio_stopLamp(0);
}